package com.epam.rd.elevate.gpr.commandlambdastream;

public interface Command {
    void execute();
}
