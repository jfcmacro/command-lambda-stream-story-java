package com.epam.rd.elevate.gpr.commandlambdastream;

public enum Region {
    NORTH, EAST, SOUTH, WEST
}
