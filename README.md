# Command Lambda Stream Story

**To read the story**: https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fcoffee-machine-story-java

**To code yourself**: https://git.epam.com/Refactoring-Stories/coffee-machine-story-java

**Estimated reading time**: 50 minutes

## Story Outline

Programming languages are used to perform a computation, let's
understand, a transformation of a value. The transformation was
explicitly described and formed the complete program.

Since the emergence of User Oriented Programming languages object
(OOP), the computation was defined explicitly, through of the source
program. If it was required to define computation in the form
dynamics, other types of languages should be used, such as the
functional languages.

With the emergence of design patterns, in particular, the pattern of
design *Command*, it was possible to define computations in a
dynamically, store them and use them when required, without
re-compiling the application and thus have the dynamic behavior.

Programming languages have evolved, and incorporated characteristics
of other paradigms in search of facilities for the programmer. That
has been the case with the incorporation of functional features into
OOP languages.

In this story we will show this evolution, through an application
which implements the main functions of a database based in
spreadsheets, through the following types of computation: `forEach`,
`filter`, `map` and `reduce`. This service we will implement starting
with the design pattern called Command, in its first version in a
non-generic way, in the second we will improve it with generic data
type mechanisms. Then in a third version we will do it through lambda
expressions. in a fourth modification, we will present lambda
expressions in a more simple through method references. Finally, we
will show as streams make it easy for us to implement these methods.



## Story Organization

**Story Branch**: main

> `git checkout main`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #command_design_pattern, #generics, #lambda_functions, #streams
